# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    @property
    def rule_pattern_shop(self):
        return {
            'shop': self.move.origin.shop.id if self.move.origin.shop else None
            }

    @classmethod
    def apply_rule(cls, lines):
        pool = Pool()
        Rule = pool.get('analytic_account.rule')

        rules = Rule.search([])

        for line in lines:
            if line.analytic_lines:
                continue
            pattern = line.rule_pattern
            if hasattr(line, 'rule_pattern_shop'):
                pattern_shop = line.rule_pattern_shop
                pattern.update(pattern_shop)
            for rule in rules:
                if rule.match(pattern):
                    break
            else:
                continue
            analytic_lines = []
            for entry in rule.analytic_accounts:
                analytic_lines.extend(
                    entry.get_analytic_lines(line, line.move.post_date))
            line.analytic_lines = analytic_lines
        super(MoveLine, cls).apply_rule(lines)
