from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval


class SaleShop(metaclass=PoolMeta):
    'Sale Shop'
    __name__ = 'sale.shop'

    analytic_account = fields.Many2One(
        'analytic_account.account', 'Analytic Account',
        domain=[('type', '=', 'normal')])


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def check_for_quotation(self):
        super(Sale, self).check_for_quotation()
        pool = Pool()
        AnalyticAccountEntry = pool.get('analytic.account.entry')

        for line in self.lines:
            if line.type != 'line' and line.analytic_accounts and not self.shop:
                continue
            if not self.shop.analytic_account:
                continue
            analytic_account = self.shop.analytic_account
            value = {
                'root': analytic_account.root,
                'account': analytic_account
            }
            new_entries = AnalyticAccountEntry.create([value])
            line.analytic_accounts = new_entries
            line.save()

    @classmethod
    def process_sale_pos(cls, sale):
        pool = Pool()
        AnalyticAccountEntry = pool.get('analytic.account.entry')

        for line in sale.lines:
            if line.type != 'line' and line.analytic_accounts and not sale.shop:
                continue
            if not sale.shop.analytic_account:
                continue
            analytic_account = sale.shop.analytic_account
            value = {
                'root': analytic_account.root,
                'account': analytic_account
            }
            new_entries = AnalyticAccountEntry.create([value])
            line.analytic_accounts = new_entries
            line.save()
        super(Sale, cls).process_sale_pos(sale)
        return sale
