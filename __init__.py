# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import move
from . import rule
from . import sale


def register():
    Pool.register(
        move.MoveLine,
        rule.Rule,
        sale.Sale,
        sale.SaleShop,
        module='analytic_sale_pos', type_='model')
